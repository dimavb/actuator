#pragma once
#include "mbed.h"
#include "QEI.h"
#include <atomic>
enum ACTUATOR
{
    ENCODER,
    ANALOG
};

class Actuator{

public:
    Actuator(PinName _Rpin, PinName _Lpin, PinName _pwm, PinName _InA,PinName _InB, PinName _cur, PinName _cs, ACTUATOR _type);
    int getIntPos();
    void goTo(int target);
    int getCurrent();
    float getPwm();
    void setPID(int P, int I, int D, int _deadBand);
private:
    float getPos();
    float actMax, actMin;
    float Kp, Ki, Kd, deadBand;
    void worker();
    int PID();
    int inited=0;
    bool calibrated=false;
    std::atomic<int> targetPos;
    int currentPos;
    float currentSpeed;
    float period;
    int analogMin=0;
    Thread * workerThread;
    AnalogIn * analog;
    AnalogIn * cur;
    DigitalOut *Rpin;
    DigitalOut *Lpin;
    DigitalOut *cs;
    PwmOut *pwm;
    QEI *enc;
    ACTUATOR type;
    Timer t;
};