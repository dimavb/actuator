/* mbed Microcontroller Library
 * Copyright (c) 2019 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */

#include "AnalogIn.h"
#define MBED_CONF_W5500_SPI_SPEED 1000000
#define MBED_CONF_WIZNET_RESET PA_3

#define MBED_CONF_TARGET_STDIO_UART_TX PB_6
#define MBED_CONF_TARGET_STDIO_UART_RX PB_7

#define NETWORK "10.0.0."
#define SERVER_ADDR "10.0.0.100"
//#define NETWORK "192.168.0."
//#define SERVER_ADDR "192.168.0.107"

#define OVERHEAT_TEMP 65

//#include "PinNames.h"
#include "mbed.h"
#include "WIZnetInterface.h"
#include "QEI.h"
#include <string>
#include "Actuator.h"
#include <vector>
#include "KVStore.h"
#include "kvstore_global_api.h"

using namespace std;

//AnalogIn currentSns (PA_1);
//addr pins
DigitalIn addr0(PB_12, PullUp);
DigitalIn addr1(PB_13, PullUp);
DigitalIn addr2(PB_14, PullUp);
DigitalIn addr3(PB_15, PullUp);
DigitalIn addr4(PA_8, PullUp);
DigitalIn addr5(PA_9, PullUp);
DigitalIn addr6(PA_10, PullUp);
DigitalIn actType(PA_11, PullUp);
//sensors input pins
//AnalogIn analogIn (PB_1);
//DigitalIn digitalIn(PB_2);
//DigitalIn digitalIn2(PB_1);

//QEI enc(PB_2, PB_1, NC, 12, QEI::X4_ENCODING);

//driver pins
//DigitalOut outA(PB_8);
//DigitalOut outB(PB_9);
//PwmOut pwm(PA_0);

//driver diag out ctrl pin
//DigitalOut sel0(PC_15);

// Network interface
//NetworkInterface *net;


DigitalOut led (PC_13);

uint8_t addr=0;

Actuator *act;
void netThread();
void statusWatcherThread();
string statusStr="";
Mutex statusStrMtx;

int main() {
    printf("Power on\n");
    for (uint8_t i=0; i<7; i++)
    {
        led=!led;
        thread_sleep_for(50);
    }
    addr= addr0.read() | addr1.read() << 1 | addr2.read() << 2 | addr3.read() << 3 | addr4.read() << 4 | addr5.read() << 5 | addr6.read() << 6;
    addr++;
    printf("Id: %d\n", addr);
    int storageInited=0;
    size_t readSize=0;
    int ret  = kv_get ("/kv/init",&storageInited,4,&readSize);
    printf ("storage inited:%d res: %d\n",storageInited, ret);
    
    if (storageInited!=1 || ret!=0)
    {
        //storage not inited
        int val=1;
        kv_set("/kv/pidP",&val,4,0);
        val=2000;
        kv_set("/kv/pidI",&val,4,0);
        val=10;
        kv_set("/kv/pidD",&val,4,0);
        val=2;
        kv_set("/kv/pidDB",&val,4,0);
        val=1;
        kv_set("/kv/init",&val,4,0);

        printf ("PID data saved\n");    
    }

    int kP=0;
    int kI=0;
    int kD=0;
    int deadBand=0;
    
    kv_get ("/kv/pidP",&kP,4,&readSize);
    kv_get ("/kv/pidI",&kI,4,&readSize);
    kv_get ("/kv/pidD",&kD,4,&readSize);
    kv_get ("/kv/pidDB",&deadBand,4,&readSize);

    //PinName _Rpin, PinName _Lpin, PinName _pwm, PinName _InA,PinName _InB, PinName _cur, PinName _cs, ACTUATOR _type
    //old board
    //  act = new Actuator(PB_8, PB_9, PA_0, PB_2, PB_1, PA_1, PC_15, ANALOG);
    //new board
    act = new Actuator(PB_9, PB_8, PA_0, PB_3, PB_1, PA_1, PC_15, ANALOG/*ACTUATOR(actType.read())*/);
    
    act->setPID(kP, kI, kD, deadBand);

    Thread hb;
    hb.start([](){
        led=0;
        while(1)
        {
            thread_sleep_for(10);
            led=!led;
            thread_sleep_for(1000);
            led=!led;
        }
    });
    
    Thread net;
    Thread statusWatcher;
    statusWatcher.start(statusWatcherThread);
    net.start(netThread);
    net.join();
    while(1)
    {
        thread_sleep_for(1000);
    }
}

void split(const string& s, char c,
           vector<string>& v) {
   string::size_type i = 0;
   string::size_type j = s.find(c);

   while (j != string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);

      if (j == string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}

void netThread()
{
    NetworkInterface *net1;
    static uint8_t WIZNET_DEFAULT_TESTMAC[6] = {0x00, 0x08, 0xdc, 0x19, 0x85, addr};
    static WIZnetInterface eth1(WIZNET_MOSI, WIZNET_MISO, WIZNET_SCK, WIZNET_CS,
                              WIZNET_RESET);
 
  nsapi_size_or_error_t result;
  nsapi_size_or_error_t send_result;
  nsapi_size_or_error_t r_result;
 
  
  string Wiz_ip = NETWORK  + to_string(addr+100);
  string Wiz_gateway = string(NETWORK) + string("1");
  string Wiz_netmask = "255.255.255.0";
  string Wiz_dnsaddr = "8.8.8.8";
  string serverIpAddr = string(SERVER_ADDR);

  uint16_t listenPort = 9000;
  uint16_t sendToPort = 8000;
  
  SocketAddress serverAddr;
  SocketAddress recvAddr;
  serverAddr.set_ip_address(serverIpAddr.c_str());
  serverAddr.set_port(sendToPort);
 
  printf("Network info \r\n");
  printf("ip      : %s \r\n", Wiz_ip.c_str());
  printf("gateWay : %s \r\n", Wiz_gateway.c_str());
  printf("netmask : %s \r\n", Wiz_netmask.c_str());
  printf("dns     : %s \r\n", Wiz_dnsaddr.c_str());
 
  eth1.init(WIZNET_DEFAULT_TESTMAC, Wiz_ip.c_str(), Wiz_netmask.c_str(), Wiz_gateway.c_str());
  //eth1.setDnsServerIP(Wiz_dnsaddr.c_str());

  net1 = &eth1;

  UDPSocket socketRecv;
  UDPSocket socketSend;
  
  printf("UDP Socket open \n");
  result  = socketRecv.open(net1);
  socketSend.open(net1);

  printf("UDP Socket bind \n");
  result = socketRecv.bind(listenPort);
  
  printf("UDP send to \n");

  send_result = socketSend.sendto(serverAddr, "$currentPos;0;", strlen("$currentPos;0;"));

  printf("UDP send result[%d]: %s\n", send_result, "$currentPos;0;");
  printf("_____________________________ \r\n");

  char buff[512];
    while (1) {
        led=0;
        memset(buff,0,512);
        r_result = socketRecv.recvfrom(&recvAddr, buff, 512);
        if (r_result > 0) {
            if (buff[0] == '$')
            {   led=1;
                //printf("[%d]recv[%s]:[%d] : %s \n",r_result, recvAddr.get_ip_address(), recvAddr.get_port(), buff);
                //printf("Got packet\n");               
                vector<string> vals;
                split (string(buff), ';',vals);
                if (vals.empty()) continue;
                
                if (vals[0]=="$moveTo" && vals.size() >= (addr+1))
                {
                    int target=stoi(vals[addr]);
                    printf("moveTo %d\n",target);
                    if (target < 0 || target > 65535) 
                    {
                        printf("Wrong position: %d", target);
                        continue;
                    }
                    act->goTo(target);
                    continue;
                }

                if (vals[0]=="$status")
                {
                    statusStrMtx.lock();
                    string status=statusStr;
                    statusStrMtx.unlock();
                    printf("status: %s\n",status.c_str());
                    
                    socketSend.sendto(serverAddr, status.c_str(), strlen(status.c_str()));
                    continue;
                }
                if (vals[0]=="$setPID")
                {
                    int val=stoi(vals[1]);
                    kv_set("/kv/pidP",&val,4,0);
                    val=stoi(vals[2]);
                    kv_set("/kv/pidI",&val,4,0);
                    val=stoi(vals[3]);
                    kv_set("/kv/pidD",&val,4,0);
                    val=stoi(vals[4]);
                    kv_set("/kv/pidDB",&val,4,0);
                    act->setPID(stoi(vals[1]),stoi(vals[2]),stoi(vals[3]),stoi(vals[4]));
                    continue;
                }

            } else printf ("Wrong packet\n");

        
        /*recvAddr.set_port(sendToPort);
        send_result = socketSend.sendto(recvAddr, buff, strlen(buff));
        printf("UDP send result[%d]: %s\r\n", send_result, buff);
        printf("_____________________________ \r\n");
        */
        }
  }
}


void statusWatcherThread()
{
    AnalogIn temp(PB_0);
    int current=0;
    int pos=0;
    int prevPos[10];

    int motorLowCurrent=0;
    int motorOverCurrent=0;
    int encoderErr=0;
    int overheat=0;
    
    while (1)
    {
        thread_sleep_for(100);
        current = act->getCurrent();
        pos = act->getIntPos();
        float VR = temp.read()*3.3;
        float VRT = 3.3-VR;
        float RT = VRT / (VR / 10000);
        float ln = log(RT/10000);
        float B = 3380;
        float TX = (1 / ((ln / B) + (1 / 298.15)));
        TX = TX - 273.15;
        if (TX>OVERHEAT_TEMP) overheat=TX; else overheat=0;
        //printf ("%d\t %d\t %d\t %d\n", motorLowCurrent, motorOverCurrent, encoderErr,(int)(TX*100));
        
        // motor should be moving
        if (act->getPwm()>0.9)
        {
            // if current is too low
            if (current<15) {
                motorLowCurrent<100 ? motorLowCurrent++ : motorLowCurrent=100;
             } else 

             {
                motorLowCurrent=0;
                if (abs(prevPos[0]-pos) < 2)
                {
                    encoderErr<100 ? encoderErr++ : encoderErr=100;
                } else encoderErr=0;
             }

            if (current>2000) {
                motorOverCurrent<100 ? motorOverCurrent++ : motorOverCurrent=100;
             } else motorOverCurrent=0;
             

        }

        for (uint8_t i=0; i<9; i++)
        {
            prevPos[i]=prevPos[i+1];
        }
        prevPos[9]=pos;

        statusStrMtx.lock();
        if (motorLowCurrent<10 &&
            motorOverCurrent<10 &&
            encoderErr<10 &&
            overheat<10)
            {
                statusStr="$currentPos;" + to_string(pos*64);// + ";" + to_string((int)TX) + ";";
            } else
            {
                statusStr="$error;";
                if (motorLowCurrent>=10) statusStr+="MotorLowCurrent_";
                if (motorOverCurrent>=10) statusStr+="MotorOverCurrent_";
                if (encoderErr>=10) statusStr+="PositionSensorError_";
                if (overheat>=10) statusStr+="Overheat_" + to_string((int)TX) + "C";
                statusStr+=";";
            }
        statusStrMtx.unlock();
    }
}
namespace mbed
{
    FileHandle *mbed_target_override_console(int)
    {
        // SerialWireOutput
        static BufferedSerial pc (PB_6, PB_7, 115200);
        return &pc;
    }
}