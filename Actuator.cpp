#include "Actuator.h"
using namespace std::chrono;

#define DITHER 4
#define MARGIN 0.005

Actuator::Actuator (PinName _Rpin, PinName _Lpin, PinName _pwm, PinName _InA,PinName _InB, PinName _cur, PinName _cs, ACTUATOR _type)
{
    type=_type;
    if (type==ENCODER)
    {
        enc=new QEI (_InA, _InB, NC, 12, QEI::X4_ENCODING);
        enc->reset();
        
    } else
    {
        analog = new AnalogIn(_InB); // Maybe ))
    }
    Lpin = new DigitalOut(_Lpin);
    Rpin = new DigitalOut(_Rpin);
    pwm= new PwmOut(_pwm);
    cur = new AnalogIn(_cur);
    cs = new DigitalOut(_cs);
    pwm->period(0.00005);
    pwm->write(0);
    Rpin->write(1);
    targetPos=0;
    workerThread = new Thread;
    workerThread->start(callback(this, &Actuator::worker));
    t.start();
    
    //set default values
    Kp=10;
    Ki=0.2;
    Kd=0.01f;
    deadBand = 2;
}

float Actuator::getPwm()
{
    return pwm->read();
}

void Actuator::goTo(int target)
{
    if (!calibrated) return;
    targetPos = target/64;
    period= period*0.9 + (t.elapsed_time().count()>500000 ? 500000 : t.elapsed_time().count())*0.1;
    //printf("Going to %d with speed %d\n",(int)targetPos, (int)period);
    t.reset();
}

int Actuator::getIntPos()
{
    if (type == ENCODER) return enc->getPulses()*41; // 1600->65535
    if (type == ANALOG) 
    {
        float valFilt=0;
        for (int i=0; i<DITHER; i++)
        {
            valFilt+=analog->read();
        }

        valFilt=((valFilt))/DITHER;
        valFilt=(valFilt-actMin)/(actMax-actMin);
        valFilt=valFilt*1024;
        if (valFilt<0) valFilt=0;
        if (valFilt>1023) valFilt=1023;
        return (int)valFilt;
    }
    return 777;
}
float Actuator::getPos()
{

    if (type == ENCODER) return enc->getPulses()*41; // 1600->65535
    if (type == ANALOG) 
    {
        float valFilt=0;
        for (int i=0; i<DITHER; i++)
        {
            valFilt+=analog->read();
        }

        valFilt=((valFilt))/DITHER;//*65535-analogMin-(65535-ANALOG_TOP_VALUE))*(65535.0f/(ANALOG_TOP_VALUE-analogMin));

        return valFilt;
    }
    return 777;
}

int Actuator::getCurrent()
{
    return int(cur->read()*3.3*1700);
}

void Actuator::setPID(int P, int I, int D, int _deadBand)
{
    Kp=(float)P/1000;
    Ki=(float)I/1000;
    Kd=(float)D/1000;
    deadBand=_deadBand;
    printf("set PID values %d %d %d %d\n",P, I, D, _deadBand);
    
}
void Actuator::worker()
{
    Timer t2;
    t2.start();
    
    int prevPos=0;
    int pos=0;
thread_sleep_for(2000);
        
    if (!inited)
    {
        //1 sec back
        Rpin->write(0);
        Lpin->write(1);
        pwm->write(1); 
        thread_sleep_for(1000);
        pwm->write(0); 
        thread_sleep_for(400);
        Rpin->write(1);
        Lpin->write(0);
        
        //forward 
        pwm->write(1); 
        thread_sleep_for(250);
        t2.reset();

        while(duration_cast<seconds>(t2.elapsed_time()).count()<60)
        {
            pos=getPos()*1024;
            if ((abs(pos-prevPos)<10 && pos > 800)) 
            {
                inited=1;
                break;
            } else {
                printf("max: %d %d for 500ms\n",(int)(pos),(int)(pos-prevPos));
            }
            prevPos=pos;
            thread_sleep_for(500);
        }
        pwm->write(0); 
        actMax=getPos()-MARGIN;
        printf("Max: %d\n",(int)(actMax*1000));
        //backwards
        Rpin->write(0);
        Lpin->write(1);
        pwm->write(1); 
        thread_sleep_for(250);
        t2.reset();

        while(duration_cast<seconds>(t2.elapsed_time()).count()<60)
        {
            pos=getPos()*1024;
            if ((abs(pos-prevPos)<10 && pos < 200)) 
            {
                inited++;
                break;
            } else {
                printf("min: %d %d for 500ms\n",(int)(pos),(int)(pos-prevPos));
            }
            prevPos=pos;
            thread_sleep_for(500);
        }
        pwm->write(0); 

        if (inited==2) 
        {
            printf("Min: %d\n", (int)(getPos()*1000));
            actMin=getPos()-MARGIN;
        }
        else 
        {
            printf("Timeout, dist: %d\n", (int)(getPos()*1000));
            actMin=0.01;
        }
        calibrated=true;

        if (type==ENCODER) enc->reset();
    }

    
    prevPos=0;
    pos=0;
    goTo(0);
    
    while(1)
    {
        float speed=PID();
        if (speed>0)
        {
            Lpin->write(0);
            Rpin->write(1);
            cs->write(1);
        } else
        {
            Rpin->write(0);
            Lpin->write(1);
            cs->write(0);
        }
        
        pwm->write(abs(speed/255));

        thread_sleep_for(5);
        if(duration_cast<milliseconds>(t2.elapsed_time()).count()>1000)
        {
            t2.reset();
            int c=0;
            for (int i=0; i< 32; i++)
            {
                c=c+getCurrent();
            }
            c=c/32;
            int trg=targetPos;
            printf("pos: %d\t target: %d \tcurrent: %d\t speed: %d\t pwm:%d\n",currentPos, trg,c, prevPos-currentPos,(int)speed);
            prevPos=currentPos;
        }
    }

}
/*int Actuator::pid()
{
    currentPos=getPos();
    int err = targetPos-currentPos;
    err=err*20;
    if (err<-255) err=-255;
    if (err>255) err=255;
    return err;
}*/
int Actuator::PID()
{
    // Calculate how far we are from the target
    currentPos=getIntPos();
    float error = targetPos - currentPos;
    static float errorlast = error;
    static float derivative;
    static float integral;
    
    /// DIRTY
    //measure with timer
    float dt=0.005f;
    //

    float maxOutput=255;

    
    
    
    
    static int output;
    int svMin = 1;
    int svMax = 1023;
    // If the error is within the specified deadband, and the motor is moving slowly enough,
    // Or if the motor's target is a physical limit and that limit is hit (within deadband margins),
    if ((abs(error) <= deadBand && abs(output) < 30)
        || (targetPos < (deadBand + 1 + (double)svMin) && currentPos < (deadBand + 1 + (double)svMin))
        || (targetPos > ((double)svMax - (1 + deadBand)) && currentPos > ((double)svMax - (1 + deadBand))))
    {
        // Stop the motor
        output = 0;
        error = 0;
    }
    else
    {
        // Else, update motor duty cycle with the newest output value
        // This equation is a simple PID control loop
        output = ((Kp * error) + (Ki * integral)) + (Kd * derivative);
        
    }


    //Prevent output value from exceeding maximum output specified by user, otherwise accumulate the integral
    if (output >= maxOutput)
        output = maxOutput;
    else if (output <= -maxOutput)
        output = -maxOutput;
    else
        integral += (error * dt);

    derivative = (error - errorlast) / dt;
    
   // printf ("i %d \td %d \te %d\n", (int)integral*1000, (int)derivative*1000,(int)error);
    return output;
}